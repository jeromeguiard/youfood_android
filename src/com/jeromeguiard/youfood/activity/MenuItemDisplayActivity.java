package com.jeromeguiard.youfood.activity;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;

import com.jeromeguiard.youfood.R;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class MenuItemDisplayActivity extends Activity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_item_display);
		
		Bundle extra = getIntent().getExtras();
	    if(extra != null ){
	        //MenuItem item = (MenuItem) extra.getCharSequence("menuItemPassed") ;   
	        TextView title =(TextView) findViewById(R.id.textTitle);
	        title.setText(extra.getString("title"));
	        TextView descriptionContent = (TextView) findViewById(R.id.textDescirptionContent);
	        descriptionContent.setText(extra.getString("description"));
	        TextView priceContent = (TextView) findViewById(R.id.textPriceContent);
	        priceContent.setText(extra.getString("price"));
	        TextView preparationTime = (TextView) findViewById(R.id.textPreparationTimeContent);
	        preparationTime.setText(extra.getString("preparationTime"));
	        ImageView image =(ImageView) findViewById(R.id.imageViewProductImage);
	        Drawable d = Drawable.createFromPath(extra.getString("photo"));
	        image.setImageDrawable(d);

	    }
		
	}
	
	
}
