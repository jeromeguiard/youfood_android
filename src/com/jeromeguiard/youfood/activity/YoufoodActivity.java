package com.jeromeguiard.youfood.activity;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.jeromeguiard.youfood.R;


public class YoufoodActivity extends Activity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {	
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

	}
	
	public void order(View view) {
       // Resources res = getResources();
        Intent intent;
        intent = new Intent().setClass(this,ClientMenuDisplayAndOrderActivity.class);
        startActivity(intent);
	}
	
	
	public void help(View view) {
	    Socket socket = null ;
	    PrintWriter out = null;
		try {
			socket = new Socket("192.168.1.108", 4444);
			out = new PrintWriter(socket.getOutputStream(), true);
			out.println("help");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	       
	}
   
}