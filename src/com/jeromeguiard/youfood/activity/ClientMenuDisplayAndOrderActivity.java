package com.jeromeguiard.youfood.activity;


import com.jeromeguiard.youfood.Menu;
import com.jeromeguiard.youfood.OrderList;
import com.jeromeguiard.youfood.activity.MenuDisplayActivity;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import com.jeromeguiard.youfood.R;
import android.widget.TabHost;

public class ClientMenuDisplayAndOrderActivity extends TabActivity {

	public static OrderList order = new OrderList();
	public static Menu menu = new Menu();

	@Override
    public void onCreate(Bundle savedInstanceState) {	
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maintab);
        Resources res = getResources();
        TabHost tabHost = getTabHost();
        TabHost.TabSpec spec;
        Intent intent;
        intent = new Intent().setClass(this,MenuDisplayActivity.class);
        spec = tabHost.newTabSpec("Menu").setIndicator("menuItems",res.getDrawable(R.drawable.ic_tab_artists)).setContent(intent);
        tabHost.addTab(spec);
        intent = new Intent().setClass(this, OrderActivity.class);
        spec = tabHost.newTabSpec("Order").setIndicator("Order",res.getDrawable(R.drawable.ic_tab_artists)).setContent(intent);
        tabHost.addTab(spec);
	}
	
}
