package com.jeromeguiard.youfood.activity;

import java.util.ArrayList;
import java.util.HashMap;

import com.jeromeguiard.youfood.SubmitOrder;

import com.jeromeguiard.youfood.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;


public class OrderActivity extends Activity  {
	TextView textview;
	private ListView listOrderItem;
	
	//@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.displayorder);
		//Button submit = (Button) findViewById(R.id.buttonSubmitOrder);
		//submit.setOnClickListener(this);
	}
    
	public void onStart(){
	    super.onStart();
    }
	
	public  void onResume() {
		super.onResume();
		listOrderItem = (ListView) findViewById(R.id.listViewOrder);
        ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String,String>>();
        HashMap<String, String> map;
        for (int i = 0; i < ClientMenuDisplayAndOrderActivity.order.getOrderDetail().size() ; i++) {	 
            map = new HashMap<String, String>();
            map.put("name", ClientMenuDisplayAndOrderActivity.order.getOrderDetail().get(i).getMenuItem().getName());
            map.put("index", new Integer(i).toString());
            map.put("id" ,new Integer(ClientMenuDisplayAndOrderActivity.order.getOrderDetail().get(i).getMenuItem().getId() ).toString());
            map.put("number", new Integer (ClientMenuDisplayAndOrderActivity.order.getOrderDetail().get(i).getQuantity()).toString());
            listItem.add(map);
            }
       SimpleAdapter mSchedule = new SimpleAdapter (this.getBaseContext(), listItem, R.layout.orderitem,  new String[] { "name", "index","id","number"},
    		                                           new int[] { R.id.nameOrder,R.id.idproduct,R.id.index,R.id.number});
       listOrderItem.setAdapter(mSchedule);
	}
	
	public void onPause(){
		super.onPause();
	}
	
	public void onStop(){
		super.onStop();
	}

	
	public void submitOrder(View v) {
	      new SubmitOrder(ClientMenuDisplayAndOrderActivity.order); 
	}
	
	public void resetOrder(View v){
		ClientMenuDisplayAndOrderActivity.order.removeAllItems();
		this.onResume();
	}
	
	public void deleteElementOrder(View view){
		LinearLayout ll = (LinearLayout) view.getParent();
		TextView tv = (TextView) ll.getChildAt(3);
		ClientMenuDisplayAndOrderActivity.order.removeOrderDetail(new Integer ((String) tv.getText()));
        this.onResume();
	}
}
