package com.jeromeguiard.youfood.activity;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jeromeguiard.youfood.DownloadImageFromServer;
import com.jeromeguiard.youfood.MenuItem;
import com.jeromeguiard.youfood.R;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;




public class MenuDisplayActivity extends /*Tab*/Activity{

	private ListView listMenuItem;

	public void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.displaymenu);
        listMenuItem = (ListView) findViewById(R.id.listView);
        ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String,String>>();
        HashMap<String, String> map;
        Gson gson = new Gson();
        JsonObject jsontest = (JsonObject) new JsonParser().parse(getJson());
        JsonArray obj2 = jsontest.getAsJsonArray("objects");
        for (int i = 0; i < obj2.size() ; i++) {
            MenuItem monmenu = gson.fromJson( obj2.get(i).getAsJsonObject().toString(), MenuItem.class);
            ClientMenuDisplayAndOrderActivity.menu.getMenu().add(monmenu);
            DownloadImageFromServer d  = new DownloadImageFromServer();
            d.downloadFile("http://192.168.1.110/media/menu/youfood_"+Integer.toString(monmenu.getId())+".jpeg", Integer.toString(monmenu.getId()));
            map = new HashMap<String, String>();
            map.put("idproduct", Integer.toString(monmenu.getId()));
            map.put("name", monmenu.getName());
            map.put("description", monmenu.getDescription());
            map.put("price", monmenu.getPrice());
            map.put("position", new Integer(i).toString());
            String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            map.put("photo",  storagePath + "/youfood_"+ Integer.toString(monmenu.getId())+".jpeg" );
            listItem.add(map);
            }
        
         
        SimpleAdapter mSchedule = new SimpleAdapter (this.getBaseContext(), listItem, R.layout.menuitem,  new String[] {"name", "description","price","idproduct","position","photo"}, new int[] {R.id.name, R.id.description,R.id.price,R.id.idproduct,R.id.position,R.id.img});
       listMenuItem.setAdapter(mSchedule);
       
    }

	public void clickImage( View view){
	    LinearLayout ll = (LinearLayout) view.getParent();
	    LinearLayout ll2 = (LinearLayout) ll.getChildAt(1);
	    TextView tv = (TextView)ll2.getChildAt(4);
	    ImageView iv = (ImageView)ll.getChildAt(0);	    
		Intent intent;
		int position = new Integer((String) tv.getText());
        intent = new Intent().setClass(this,MenuItemDisplayActivity.class);
        intent.putExtra("title", ClientMenuDisplayAndOrderActivity.menu.getMenu().get(position).getName());
        intent.putExtra("id", ClientMenuDisplayAndOrderActivity.menu.getMenu().get(position).getId());
        intent.putExtra("description", ClientMenuDisplayAndOrderActivity.menu.getMenu().get(position).getDescription());
        intent.putExtra("price", ClientMenuDisplayAndOrderActivity.menu.getMenu().get(position).getPrice());
        intent.putExtra("preparationTime", new Float(ClientMenuDisplayAndOrderActivity.menu.getMenu().get(position).getPreparationTime()).toString());
        String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        intent.putExtra("photo", storagePath + "/youfood_"+ Integer.toString(ClientMenuDisplayAndOrderActivity.menu.getMenu().get(position).getId())+".jpeg");
        startActivity(intent);
	}
	
	public void addItem(View view){
		LinearLayout ll = (LinearLayout)view.getParent();
		LinearLayout ll2 = (LinearLayout)ll.getChildAt(1);
		TextView tv = (TextView)ll2.getChildAt(3);
		int id = new Integer((String) tv.getText());
		ClientMenuDisplayAndOrderActivity.order.addOrderDetail(ClientMenuDisplayAndOrderActivity.menu.getMenuItemById(id)
				, new Integer((String)((TextView) ll.getChildAt(4)).getText()));
		
	}
	
	public void numberLess(View v){
		LinearLayout ll = (LinearLayout) v.getParent();
		TextView tv =(TextView) ll.getChildAt(4);
		tv.setText(new Integer(new Integer((String) tv.getText()) - 1).toString());
		this.onResume();
	}
	public void numberMore(View v){
		LinearLayout ll = (LinearLayout) v.getParent();
		TextView tv =(TextView) ll.getChildAt(4);
		tv.setText(new Integer(new Integer((String) tv.getText()) + 1).toString());
		this.onResume();
	}
	
	
    public String getJson(){
	    StringBuilder builer = new StringBuilder();
	    HttpClient client = new DefaultHttpClient();
	 // HttpGet httpGet = new HttpGet("http://youfood.com/api/menuItem/?format=json");
	    HttpGet httpGet = new HttpGet("http://192.168.1.110/api/v1/menuItem/?format=json");
	    try{
		    HttpResponse response = client.execute(httpGet);
		    StatusLine statusLine =response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
		    if(statusCode == 200){
			    HttpEntity entity = response.getEntity();
			    InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content)); 
                String line;
                while ((line = reader.readLine()) != null){
				    builer.append(line);
			    }
            } else {
			    Log.e(Gson.class.toString(), "Failed to dl file");
		  }
	  } catch (ClientProtocolException e){
		  e.printStackTrace();
	  } catch (Exception e) {
		e.printStackTrace();
	}
	  return builer.toString();
  }
    
    Drawable DownloadDrawable(String url, String src_name) throws java.io.IOException {
    	return Drawable.createFromStream(((java.io.InputStream) new java.net.URL(url).getContent()), src_name);
    }
    
    
    
}
