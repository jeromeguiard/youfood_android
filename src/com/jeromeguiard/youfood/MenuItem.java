package com.jeromeguiard.youfood;

import android.graphics.drawable.Drawable;

public class MenuItem {

	private int id;
	private String name;
	private String description;
	private Float price;
	private float preparationTime;
	private String photo;
	private Drawable photoDrw;
	
	//private String photoPath;
	public MenuItem(){}
	
	public Drawable getPhotoDrw() {
		return photoDrw;
	}

	public void setPhoto(Drawable photo) {
		this.photoDrw = photo;
		
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrice() {
		return price.toString();
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getPreparationTime() {
		return preparationTime;
	}

	public void setPreparationTime(float preparationTime) {
		this.preparationTime = preparationTime;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}




	
	
}
