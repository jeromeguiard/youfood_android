package com.jeromeguiard.youfood;

public class OrderDetail {
	private int id;
    private MenuItem menuItem;
    private int quantity;
    
    public OrderDetail() {
	    this.setQuantity(1);
	}
    
    public OrderDetail(MenuItem menuItem, int quantity){
    	this.setQuantity(quantity);
    	this.setMenuItem(menuItem);
    }
    
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public MenuItem getMenuItem() {
		return menuItem;
	}
	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}
    
	public String getMenuItemUri(){
		return "/api/v1/menuItem/" + new Integer(this.getMenuItem().getId()).toString()+"/";
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
