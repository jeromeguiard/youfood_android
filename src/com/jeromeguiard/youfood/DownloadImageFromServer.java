package com.jeromeguiard.youfood;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;


import android.os.Environment;

public class DownloadImageFromServer {
     private URL  url ;
//	 private Bitmap bmImg;
	 private InputStream input;
	 public DownloadImageFromServer(){		 
		 
	 }
	 
	public void downloadFile(String fileUrl, String imageId){

		try {
		
			  url = new URL (fileUrl);
				 input = url.openStream();
		
		   
		    String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath();
		    //OutputStream output = new FileOutputStream (storagePath + "/youfood_"+ imageId+".jpeg");
		    OutputStream output = new FileOutputStream (storagePath + "/youfood_"+ imageId+".jpeg");
		    
		    try {
		        byte[] buffer = new byte[10];
		        int bytesRead = 0;
		        while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
		            output.write(buffer, 0, bytesRead);
		        }
		    } 
		    catch (Exception e) {
			}
		    finally {
		        output.close();
		    }
		}
		catch (Exception e) {
		}finally {
		    try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
