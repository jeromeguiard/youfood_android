package com.jeromeguiard.youfood;

import java.util.ArrayList;
import java.util.List;

public class OrderList {
    private List<OrderDetail> listItems;
    private int table_number;
    private int id ;
    
    public OrderList(){
        this.listItems = new ArrayList<OrderDetail>();
    }
    
	public int getTable_number() {
		return table_number;
	}
	public void setTable_number(int table_number) {
		this.table_number = table_number;
	}

	public List<OrderDetail> getOrderDetail() {
		return listItems;
	}

	public void setOrderDetail(List<OrderDetail> orderDetail) {
		this.listItems = orderDetail;
	}
	
	public void addOrderDetail(MenuItem menuItem, int numberOfItem){
		this.getOrderDetail().add(new OrderDetail(menuItem,numberOfItem));
	}

	public void removeOrderDetail(int menuDetailId){
	    int i =0;
		while(i < this.getOrderDetail().size()){
			if(this.getOrderDetail().get(i).getMenuItem().getId() == menuDetailId){
				//MenuItem mi = this.getOrderDetail().get(i).getMenuItem();
				this.getOrderDetail().remove(i);
			}
			i++;
		}
	}

	public void removeAllItems(){
		int i  = this.getOrderDetail().size() -1 ;
		while (i >= 0){
		    this.getOrderDetail().remove(i);
		    i--;
		}
	}
	
	public int getId() {
		return id;
		
	}

	public void setId(int id) {
		this.id = id;
	}


}
