package com.jeromeguiard.youfood;

import java.util.ArrayList;
import java.util.List;

public class Menu {

	private List<MenuItem> menu;

	public Menu(){
		menu=new ArrayList<MenuItem>();
	}
	
	
	public List<MenuItem> getMenu() {
		return menu;
	}

	public void setMenu(List<MenuItem> menu) {
		this.menu = menu;
	}  
	
	public MenuItem getMenuItemById(int id){
		int i = 0;
		while(i<this.getMenu().size()){
			if(this.getMenu().get(i).getId() == id ){
				return this.getMenu().get(i);
			}
			i++;
		}
		return null;
	}
}
