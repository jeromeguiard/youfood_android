package com.jeromeguiard.youfood;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.google.gson.Gson;
import android.util.Log;

public class SubmitOrder {
   	
	public class SubmitGsonOrderList{
	    private List<SubmitGsonOrderDetail> listItems;
	    
	    private int table_number;
	   
	    SubmitGsonOrderList(OrderList orderList){
	    	int i=0;
	    	listItems = new ArrayList<SubmitGsonOrderDetail>();	    	
	        while(i < orderList.getOrderDetail().size()){
	        	listItems.add(new SubmitGsonOrderDetail(orderList.getOrderDetail().get(i)));
	        	i++;
	        }
	        this.setTable_number(orderList.getTable_number());
	        
	    }
    
		public int getTable_number() {
			return table_number;
		}
		public void setTable_number(int table_number) {
			this.table_number = table_number;
		}	
	}
	
	public class SubmitGsonOrderDetail{
		private String item;
		private int quantity;
		public SubmitGsonOrderDetail(OrderDetail orderDetail){
			this.setQuantity(orderDetail.getQuantity());
			this.setItem(orderDetail.getMenuItemUri());
		}
		
		public int getQuantity() {
			return quantity;
		}
		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}
		public String getItem() {
			return item;
		}
		public void setItem(String item) {
			this.item = item;
		}
		
	}
	
	 public SubmitOrder(OrderList orderList){
		 int TIMEOUT_MILLISEC = 5000;
		 HttpParams httpParams = new BasicHttpParams();
		 HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
		 HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
		 HttpClient client = new DefaultHttpClient(httpParams);
		 HttpPost request = new HttpPost("http://192.168.1.110/api/v1/orderList/");
         StringEntity stringEntity;
		try {
			stringEntity = new StringEntity(new Gson().toJson(new SubmitGsonOrderList(orderList)));
	         stringEntity.setContentType("application/json");
	         request.setEntity(stringEntity);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		 try {
			HttpResponse response = client.execute(request);
			if (response.getStatusLine().getStatusCode() == 201){
		        Log.e("qh qh qh",response.getStatusLine().getReasonPhrase());
		       }
			else{
			}
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	 
	 
	 }
}
